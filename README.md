# Client 
A Monero multisig wallet system

## Pages 

### Intro 
The intro page is the first to be displayed. It checks if the auth token is saved in localStorage, downloads user data and redirects the user to Home. If the token isn't present, the user is redirected to the login page.

### Login
Perform sign-in and redirects the user to Home.

### Home 
Downloads all payments from the server, then for each payment performs a cycle.

#### Cycle 
- ***step 1:*** check if the wallet file is created, if not create it
- ***step 2:*** downloads wallet status from server
- ***step 3:*** calls prepare, make or finalize multisig RPC and sends the resulting data to the server
- ***step 4:*** saves wallet data in the component state and renders

### Info 
Displays info about a specific payment. If the linked wallet contains funds, the user can unlock the funds and send them to the merchant (or to the customer as in a refund) via creating the transaction locally, signing and sending the signature to the server that will relay it to the blockchain.

## Data Objects 

### Payment Request 
Any payment created by a merchant or a marketplace on behalf of a merchant via Oauth API. It can be **ESCROW** or **FINALIZE EARY** and contain a description in text or json array. It is tied to a specific merchant but can be tied to any other user as client. The client cannot be changed once engaged.
A signed url is generated to allow access to the checkout page for the client.

### Multisig Wallet 
An object containing the status of the local Monero multisig wallet and store data sent by the client peers. On each step, data about the preceding step is deleted to avoid possible hacks.

### Dispute 
A moderated controversy among two parties involved in a payment requesting an external neutral point of view to determine whether a claim is correct or not based on data provided by the two parties (tracking codes, package pics, etc). 

## Statuses 

### Payment 
_Created_ - payment has been created 

_Expired_ - payments automatically expire after two days of no customer lands on the checkout page 

_Cancelled_ - by the merchant or by the user during checkout 

_Confirming_ - user claims to have submitted the payment but the incoming transaction hasn't been detected yet 

_Funded_ - funds are in the escrow wallet 

_Closed_ - payment has been completed by sending the funds to the merchant 

_Refunded_ - payment has been refunded, funds have been sent back to the user 

_Reported_ - user or merchant asked for moderation 

_ClientPayout_ - moderator decided to send funds to the user 

_MerchantPayout_ - moderator decided to send funds to the merchant 

### Wallet 
_Created_ - Wallet has been created on the server and RPC call prepare_multisig has been run on the server. Clients have to create the wallet locally, run RPC function _prepare_multisig_ and send the resulting data to the server. 

_Prepared_ - Both clients have sent _prepare_multisig_ return data. The server can now run _make_multisig_.
Clients can now get _prepare_multisig_ data from their peers and run _make_multisig_ sending the resulting data to the server. 

_Made_ - Both clients have sent _make_multisig_ return data. Now the server can run _finalize_multisig_ and fill in the final wallet address.
Clients download _make_multisig_ return data from their peers and run _finalize_multisig_ sending the resulting data to the server. 

_Finalized_ - The server verifies if the addresses sent by the peers match with the one saved locally. If all three are the same, the wallet is marked as finalized and can be used. Otherwise, the wallet is corrupted and the procedure has to be restarted. Clients check if the wallet is marked as finalized and, if so, display the info INFO button. 

## Server API 
V1.0.0

### General 
* [GET/POST] _/login_ (username, password) - perform login

### Multisig Wallet 
*ID: payment_id*

* [GET] _/multisig/create/:ID_ - get data about the multisig wallet
* [POST] _/multisig/create/:ID_ - send data about the multisig wallet

* [GET] _/multisig/sync/:ID_ - get sync data about the multisig wallet
* [POST] _/multisig/sync/:ID_ - send sync data about the multisig wallet

* [GET] _/multisig/transfer/:ID_ - get transfer data about the multisig wallet
* [POST] _/multisig/transfer/:ID_ - send transfer data about the multisig wallet

* [GET/POST] _/whoami_ - get data about the logged-in user
