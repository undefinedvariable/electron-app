import React from 'react';
import { Redirect } from "react-router-dom";
import { Mutex } from 'async-mutex';
import Payment from './Payment';
import MoneroRPC from '../utils/monero_rpc';
import './home.css';
import '../index.css';

export default class Home extends React.Component {
    rpc;
    api;

    constructor(props) {
        super(props);

        this.rpc = new MoneroRPC(this.props.config.rpc_user, this.props.config.rpc_pass, this.props.config.daemon_address+':'+this.props.config.daemon_port);
        this.api = this.props.api;
        this.notification = this.props.notification;

        this.state = {
            payments: [],
            escrow_payments: [],
            interval: null,
            loading: true,
            serverError: false,
            redirect: '',
            param: this.props.match.params,
            server_error: false,
            daemon_error: false,
            rpc_error: false,
            rpc_error_msg: '',
            wsMutex: new Mutex(),
        };

        this.logout = this.logout.bind(this);

        window.electron.log.log('Entering class HOME');
    }

    updateProgressbar(id, value) {
        const paymentsEdited = this.state.payments.slice();
        paymentsEdited.find(function (element) {
            return element.id === id;
        }).progressbar = value;
        this.setState({payments: paymentsEdited})
    }

    markWalletReady(id) {
        const paymentsEdited = this.state.payments.slice();
        paymentsEdited.find(function (element) {
            return element.id === id;
        }).wallet_ready = true;
        this.setState({payments: paymentsEdited})
    }

    componentWillUnmount() {
        if(this.state.interval != null)
            clearInterval(this.state.interval);
    }

    async componentDidMount() {
        let response = await this.api.get('payments');
        if (response.status === 200) {
            response.data.message.forEach(function (element) {
                element.wallet_filename = element.id+'_wallet';
                element.wallet_ready = false;
                element.progressbar = -1;
            });

            let allPayments = response.data.message;
            let escrowPayments = allPayments.filter((element) => element.payment_type === 'escrow');

            this.setState({payments: allPayments, escrow_payments: escrowPayments});
        } else {
            if(response.status === 401) {
                this.setState({redirect: 'login'});
            }

            window.electron.log.error('API Error 1');
            window.electron.log.error(response.status);
            window.electron.log.error(response.data);

            this.setState({loading: false, serverError: true});

            return;
        }

        // Check if local or remote node is in sync
        let moneroOk = await this.rpc.check_remotenode_sync();
        if(!moneroOk) { 
            this.setState({daemon_error: true, loading: false});
            return;
        }

        await this.performWalletUpdate();
        let intervalId = setInterval(async () => {
            await this.performWalletUpdate();
        }, 15000);
        this.setState({interval: intervalId});

        // we have finished loaded
        this.setState({loading: false});
    }

    async performWalletUpdate() {
        this.state.escrow_payments.forEach(async (payment) => {
            await this.state.wsMutex.runExclusive(async () => {

                window.electron.log.log('START PROCESSING PAYMENT ID#'+payment.id);

                let multisig;
                let response = await this.api.get('multisig/create/'+payment.id);
                if (response.status === 200) {
                    multisig = response.data.message; // TODO check what happens if payment not found or wallet not created yet
                } else {
                    if(response.status === 422 || response.status === 404) return;

                    window.electron.log.error('API error 2');
                    window.electron.log.error(response.status);
                    window.electron.log.error(response.data);

                    this.setState({server_error: true});
                    return;
                }

                // Open wallet or create new if it doesn't exist
                window.electron.log.log('['+payment.id+'] opening wallet...');
                try {
                    await this.rpc.open_wallet(payment.wallet_filename, "password");
                    window.electron.log.log('['+payment.id+'] ...ok');
                } catch(error) {
                    window.electron.log.warn('['+payment.id+'] ...fail, fallback to creating it');
                    window.electron.log.log('['+payment.id+'] creating wallet...');
                    try {
                        await this.rpc.create_wallet(payment.wallet_filename, "password");
                        window.electron.log.log('['+payment.id+'] ...ok');
                    } catch(error) {
                        window.electron.log.error(error);
                        window.electron.log.error('['+payment.id+'] ...fail');
                        this.setState({rpc_error: true, rpc_error_msg: error});

                        return;
                    }
                }

                // Process through multisig wallet creation steps
                switch(multisig.status)
                {
                    case 'created':
                        window.electron.log.log('['+payment.id+'] status CREATED');
                        this.updateProgressbar(payment.id, 25);

                        if(multisig.multisig_info === null)
                        {
                            let ms_info;
                            try {
                                window.electron.log.log('['+payment.id+'] preparing...');
                                ms_info = await this.rpc.prepare_multisig();
                                window.electron.log.log('['+payment.id+'] ...ok');
                            } catch(error) {
                                window.electron.log.error('['+payment.id+'] Monero fail:');
                                window.electron.log.error(error);
                                this.setState({rpc_error: true, rpc_error_msg: error});
                                break;
                            }

                            window.electron.log.log('['+payment.id+'] sending data to server...');
                            let response = await this.api.post('multisig/create/'+payment.id, {"multisig_info": ms_info});
                            if (response.status === 200) {
                                window.electron.log.log('['+payment.id+'] ...ok');
                            } else {
                                window.electron.log.error('['+payment.id+'] API fail preparing step');
                                window.electron.log.error(response.status);
                                window.electron.log.error(response.data);
                                this.setState({server_error: true});
                            }
                        }
                    break;

                    case 'prepared':
                        window.electron.log.log('['+payment.id+'] status PREPARED');
                        this.updateProgressbar(payment.id, 50);

                        if(multisig.multisig_info === null)
                        {
                            let ms_info;
                            try {
                                window.electron.log.log('['+payment.id+'] making...');
                                ms_info = await this.rpc.make_multisig(multisig.multisig_info_peers, "password");
                                window.electron.log.log('['+payment.id+'] ...ok');
                            } catch(error) {
                                window.electron.log.error('['+payment.id+'] Monero fail:');
                                window.electron.log.error(error);
                                this.setState({rpc_error: true, rpc_error_msg: error});
                                break;
                            }

                            window.electron.log.log('['+payment.id+'] sending data to server...');
                            let response = await this.api.post('multisig/create/'+payment.id, {"multisig_info": ms_info.getMultisigHex()});
                            if (response.status === 200) {
                                window.electron.log.log('['+payment.id+'] ...ok');
                            } else {
                                window.electron.log.error('['+payment.id+'] API fail making step');
                                window.electron.log.error(response.status);
                                window.electron.log.error(response.data);
                                this.setState({server_error: true});
                            }
                        }
                    break;

                    case 'made':
                        window.electron.log.log('['+payment.id+'] status MADE');
                        this.updateProgressbar(payment.id, 75);

                        if(multisig.multisig_address === null)
                        {
                            let ms_info;
                            try {
                                window.electron.log.log('['+payment.id+'] finalizing...');
                                ms_info = await this.rpc.finalize_multisig(multisig.multisig_info_peers, "password");
                                window.electron.log.log( '['+payment.id+'] ...ok');
                            } catch(error) {
                                window.electron.log.error('['+payment.id+'] Monero fail:');
                                window.electron.log.error(error);
                                this.setState({rpc_error: true, rpc_error_msg: error});
                                break;
                            }

                            window.electron.log.log('['+payment.id+'] sending data to server...');
                            let response = await this.api.post('multisig/create/'+payment.id, {"multisig_address": ms_info.getAddress()});
                            if (response.status === 200) {
                                window.electron.log.log('['+payment.id+'] ...ok');
                            } else {
                                window.electron.log.error('['+payment.id+'] API fail finalizing step');
                                window.electron.log.error(response.status);
                                window.electron.log.error(response.data);
                                this.setState({server_error: true});
                            }
                        }
                    break;

                    case 'finalized':
                        window.electron.log.log('['+payment.id+'] status FINALIZED');
                        this.updateProgressbar(payment.id, 100);
                        setTimeout(() => this.markWalletReady(multisig.payment_request_id), 3000);
                    break;

                    default:
                        window.electron.log.error('['+payment.id+'] invalid status');
                        this.updateProgressbar(payment.id, 0);
                }
                window.electron.log.log('END PROCESSING PAYMENT ID#'+payment.id);
            });
        });
    }

    logout() {
        this.setState({redirect: 'login'});
    }

    render() {
        const { payments, loading, server_error, daemon_error, rpc_error, rpc_error_msg } = this.state;

        let user = JSON.parse(localStorage.getItem('user'));

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <>

            {loading &&
                <div className="loader" id="loader2">
                <span></span>
                <span></span>
                <span></span>
            </div>
            }

            {!loading &&
            <>
                {server_error &&
                    <div className="alert alert-danger" role="alert">
                        Server error, retry
                    </div>
                }

                {daemon_error &&
                    <div className="alert alert-danger" role="alert">
                        Monero daemon not in sync
                    </div>
                }

                {rpc_error &&
                    <div className="alert alert-danger" role="alert">
                        {rpc_error_msg.toString().substr(0, rpc_error_msg.toString().indexOf('Request:')) }
                    </div>
                }

                {!server_error && !rpc_error && !daemon_error &&
                <>

                    <div className="fixed-top d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
                        <h5 className="my-0 mr-md-auto font-weight-normal">Hello <b>{user.username}</b>!</h5>
                        <nav className="my-2 my-md-0 mr-md-3">
                            <a className="p-2 text-dark" href="/settings">Settings</a>
                        </nav>
                        <button className="btn btn-outline-danger" type="button" onClick={this.logout}>Logout</button>
                    </div>

                    <div className="container">

                        <div className="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                            <h2 className="display-4">My Payments</h2>
                        </div>

                        {user.account === 'merchant' &&
                        <>
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item active" aria-current="page">As Merchant</li>
                                </ol>
                            </nav>
                            <div className="card-deck mb-3 text-center">
                            {
                                payments.filter(p => {
                                    return p.merchant === user.username;
                                }).length === 0
                                ? 
                                <div className="card">
                                    <div className="card-body">
                                        Empty
                                    </div>
                                </div>
                                : payments.filter(p => {
                                    return p.merchant === user.username;
                                }).map(payment => (
                                    <Payment payment={payment} key={payment.id} />
                                ))
                            }
                            </div>
                        </>
                        }

                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item active" aria-current="page">As Client</li>
                            </ol>
                        </nav>
                        <div className="card-deck mb-3 text-center">
                        {
                            payments.filter(p => {
                                return p.customer === user.username;
                            }).length === 0
                            ? 
                            <div className="card">
                                <div className="card-body">
                                    Empty
                                </div>
                            </div>
                            : payments.filter(p => {
                                return p.customer === user.username;
                            }).map(payment => (
                                <Payment payment={payment} key={payment.id} />
                            ))
                        }
                        </div>
                    </div>
                </>
                }
            </>
            }
        </>
        );
    }
}
