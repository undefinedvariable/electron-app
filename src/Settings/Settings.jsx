import React from 'react';
import { Redirect } from "react-router-dom";
import './settings.css';

export default class Settings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: '',
            daemon_address: props.config.daemon_address,
            daemon_port: props.config.daemon_port,
            server_address: props.config.server_address,
            socks_host: props.config.socks_host,
            socks_port: props.config.socks_port,
        };

        this.onFormChange = this.onFormChange.bind(this);
        this.saveConfig = this.saveConfig.bind(this);

        window.electron.log.log('Entering class SETTINGS');
    }

    saveConfig() {
        let config = {
            daemon_address: this.state.daemon_address,
            daemon_port: this.state.daemon_port,
            server_address: this.state.server_address,
            socks_host: this.state.socks_host,
            socks_port: this.state.socks_port,
        };
        this.props.onConfigChange(config);
    }

    onFormChange(e) {
        var state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <form className="form-signin" onSubmit={this.onSubmit} autoComplete="off">
                <div className="text-center mb-4">
                    <img className="mb-4" src={window.location.origin + '/logo.png'} alt="logo" width="80" height="89" />
                    <h1 className="h3 mb-3 font-weight-normal">Settings</h1>
                </div>

                <div className="form-label-group">
                    <input id="daemon_address" name="daemon_address" type="url" pattern="http://.*" value={this.state.daemon_address} className="form-control" onChange={this.onFormChange} placeholder="Monero Daemon Address" required autoFocus />
                    <label htmlFor="daemon_address">Monero Daemon Address</label>
                </div>

                <div className="form-label-group">
                    <input id="daemon_port" name="daemon_port" type="number" value={this.state.daemon_port} className="form-control" onChange={this.onFormChange} placeholder="Monero Daemon Port" required />
                    <label htmlFor="daemon_port">Monero Daemon Port</label>
                </div>

                <div className="form-label-group">
                    <input id="server_address" name="server_address" type="url" pattern="http://.*" value={this.state.server_address} className="form-control" onChange={this.onFormChange} placeholder="Server Address" required />
                    <label htmlFor="server_address">Server Address</label>
                </div>

                <div className="form-label-group">
                    <input id="socks_host" name="socks_host" type="text" value={this.state.socks_host} className="form-control" onChange={this.onFormChange} placeholder="Socks Host" required />
                    <label htmlFor="socks_host">Socks Host</label>
                </div>

                <div className="form-label-group">
                    <input id="socks_port" name="socks_port" type="number" value={this.state.socks_port} className="form-control" onChange={this.onFormChange} placeholder="Socks Port" required />
                    <label htmlFor="socks_port">Socks Port</label>
                </div>

                <a href="/home" className="btn btn-lg btn-primary btn-block" onClick={this.saveConfig}>Save and Go Back</a>

                <p className="mt-5 mb-3 text-muted text-center">&copy; 2017-2020</p>
            </form>
        );
    }
}
