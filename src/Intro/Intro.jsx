import React from 'react';
import { Redirect } from "react-router-dom";
import MoneroRPC from '../utils/monero_rpc';
import './intro.css';
import '../index.css'

export default class Intro extends React.Component {
    constructor(props) {
        super(props);

        this.rpc = new MoneroRPC(this.props.config.rpc_user, this.props.config.rpc_pass, this.props.config.daemon_address+':'+this.props.config.daemon_port);
        this.api = this.props.api;

        this.state = {
            redirect: '',
            daemon_error: false,
        };

        window.electron.log.log('Entering class INTRO');
    }

    async componentDidMount() {
        localStorage.removeItem('user');

        // Check if local or remote node is in sync
        let moneroOk = await this.rpc.check_remotenode_sync();
        if(!moneroOk) { 
            this.setState({daemon_error: true});
            window.electron.log.error('Monero daemon not in sync');

            return;
        }
        window.electron.log.log('monero node ok');

        try {
            let response = await this.api.get('whoami');
            
            if(response.status === 200) {
                localStorage.setItem('user', JSON.stringify(response.data.message.user));
                this.setState({ redirect: "/home" });
            } else {
                this.setState({ redirect: "/login" });
            }
        } catch(error) {
            alert('connection error, check tor and restart the program');
        }
    }

    render() {
        const { daemon_error } = this.state;

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return (
            <>
                {daemon_error &&
                    <div className="alert alert-danger" role="alert">
                        Monero daemon not in sync
                    </div>
                }

                {!daemon_error &&
                <div className="loader" id="loader2">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                }
            </>
        );
    }
}
