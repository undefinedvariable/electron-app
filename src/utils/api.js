export default class Api {
    constructor(ipcRenderer, base_url) {
        this.ipcRenderer = ipcRenderer;
        this.base_url = base_url;
    }

    async get(url, options) {
        const full_url = this.base_url + '/api/v1/' + url;
        const default_options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token') || '',
            }
        };
        const all_options = {...default_options, ...options};

        const result = await this.ipcRenderer.invoke('tor-get', full_url, all_options);

        if(result === null) throw new Error('tor connection error');

        return result;
    }

    async post(url, data, options) {
        const full_url = this.base_url + '/api/v1/' + url;
        const default_options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token') || '',
            },
        };
        const all_options = {...default_options, ...options};

        const result = await this.ipcRenderer.invoke('tor-post', full_url, data, all_options);

        if(result === null) throw new Error('tor connection error');

        return result;
    }
}
