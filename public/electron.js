const { app, BrowserWindow, ipcMain, Notification } = require("electron");
const isDev = require('electron-is-dev');
const Store = require('electron-store');
const fs = require("fs");
const log = require('electron-log');
const path = require('path');
const { spawn } = require("child_process");
const tor_axios = require('tor-axios');

function randomString (len, bits) {
    bits = bits || 36;
    var outStr = "", newStr;
    while (outStr.length < len)
    {
        newStr = Math.random().toString(bits).slice(2);
        outStr += newStr.slice(0, Math.min(newStr.length, (len - outStr.length)));
    }
    return outStr;
}

// config
const schema = {
    daemon_address: {
        type: 'string',
        default: 'http://stagenet.community.xmr.to',
        format: 'url'
    },
    daemon_port: {
        type: 'number',
        minimum: 2000,
        maximum: 40000,
        default: 38081
    },
    server_address: {
        type: 'string',
        default: 'http://135.181.109.206',
    },
    rpc_user: {
        type: 'string',
        default: randomString(12),
    },
    rpc_pass: {
        type: 'string',
        default: randomString(12),
    },
    socks_host: {
        type: 'string',
        default: 'localhost',
    },
    socks_port: {
        type: 'number',
        default: 9050,
    }
};
const config = new Store({schema});

// proxy
const tor = tor_axios.torSetup({
    ip: config.socks_host,
    port: config.socks_port,
});

// IPC
ipcMain.handle('set-config', async (event, newConfig) => {
    config.set(newConfig);
    return config.store;
});

ipcMain.on('get-config', (event, _args) => {
    event.returnValue = config.store;
});

ipcMain.handle('notify', (event, title, body) => {
    const notification = {
        title: title,
        body: body
    }
    new Notification(notification).show();
});

ipcMain.handle('tor-get', async (event, url, options) => {
    try {
        const response = await tor.get(url, options);
        return {status: response.status, data: response.data};
    } catch(error) {
        if(error.response === undefined) return null;
        return {status: error.response.status, data: error.response.data};
    }
});

ipcMain.handle('tor-post', async (event, url, data, options) => {
    try {
        const response = await tor.post(url, data, options);
        return {status: response.status, data: response.data};
    } catch(error) {
        if(error.response === undefined) return null;
        return {status: error.response.status, data: error.response.data};
    }
});

// monero-wallet-rpc
const out = fs.openSync(app.getAppPath() + "/monero-wallet-rpc.log", "a");
const err = fs.openSync(app.getAppPath() + "/monero-wallet-rpc.log", "a");

const parameters = [
    "--rpc-bind-port", "18083",
    "--daemon-address", config.get('daemon_address')+':'+config.get('daemon_port'), 
    "--stagenet",
    "--rpc-login", config.get('rpc_user')+':'+config.get('rpc_pass'),
    "--wallet-dir", "client_wallets",
    "--rpc-access-control-origins", "http://localhost:4000", // TODO replace 
];

const moneroWalletRpc = spawn(app.getAppPath() + "/monero-wallet-rpc", parameters, {
    detached: true,
    stdio: ["ignore", out, err],
});

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 900,
        height: 680,
        title: "Honoraium Client",
        webPreferences: {
            nodeIntegration: false, // is default value after Electron v5
            contextIsolation: true, // protect against prototype pollution
            enableRemoteModule: false, // turn off remote
            preload: path.join(__dirname, "preload.js") // use a preload script
        }
    });
    mainWindow.loadURL(
        isDev
        ? 'http://localhost:4000' 
        : `file://${path.join(__dirname, '../build/index.html')}`
    );
    mainWindow.on("closed", () => (mainWindow = null));
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("quit", () => {
    moneroWalletRpc.kill();
});

app.on("activate", () => {
    if (mainWindow === null) {
        createWindow();
    }
});
